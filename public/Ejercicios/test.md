# Parrafos y saltos de lineas

Para hacer

dos lineas

O utilizar  
doble espacio al final

# Titulo h1
## Titulo h2
### Titulo h3
#### Titulo h4
##### Titulo h5
###### Titulo h6

Titulo 1
===
Titulo 2
___

# Negritas y Cursivas

**negrita**
__negrita__
*cursiva*
_cursiva_
***negrita y cursiva***

# Listas
- Lista
- Lista
    * Lista
    * Lista
        + Lista
        + Lista

1. Lista
    1. Lista
        1. Lista
1. Lista
1. Lista
1. Lista
1. Lista

# Codigo Insertado
~~~
print("Hola");
~~~
~~~ kt
System.out.print("Hola");
~~~
~~~ c#
Console.WriteLine("Hola");
~~~
```html
<html>
    <head></head>
    <body></body>
</html>
```

# Enlaces
[Enlace](http://google.com)

[Enlace a Blog][blog]  [blog]:http://www.blogger.com

<http://www.blogger.com>

# Imagenes

![Texto alternativo](http://google.com)

# Tablas

|Usuario|correo|ciudad|
|-------|------|-------|
|juan  |juan@juanitojuan.com| Golondrina|
|Jaime|jaime@me.com | Estocolmo|

# Enlaces

[Enlaces](#parrafos-y-saltos-de-lineas)