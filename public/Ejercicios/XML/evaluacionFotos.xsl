<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/evaluacion">
    <html>
    <body>
        <h1>M04- Notas</h1>
        <table style="border: 1px solid black">
            <tr>
                <th style="border: 1px solid black;background-color:green">Nombre</th>
                <th style="border: 1px solid black;background-color:green">Apellidos</th>
                <th style="border: 1px solid black;background-color:green">Telefono</th>
                <th style="border: 1px solid black;background-color:green">Repetidor</th>
                <th style="border: 1px solid black;background-color:green">Nota Practica</th>
                <th style="border: 1px solid black;background-color:green">Nota Examen</th>
                <th style="border: 1px solid black;background-color:green">Nota Total</th>
                <th style="border: 1px solid black;background-color:green">Fotografia</th>
            </tr>
            <xsl:for-each select="alumno">
                <xsl:sort select="appellidos" order="descending"></xsl:sort>
                <tr>
                    <th style="border: 1px solid black"><xsl:value-of select="nombre"></xsl:value-of></th>
                    <th style="border: 1px solid black"><xsl:value-of select="apellidos"></xsl:value-of></th>
                    <th style="border: 1px solid black"><xsl:value-of select="telefono"></xsl:value-of></th>
                    <th style="border: 1px solid black"><xsl:value-of select="@repite"></xsl:value-of></th>
                    <th style="border: 1px solid black"><xsl:value-of select="notas/practicas"></xsl:value-of></th>
                    <th style="border: 1px solid black"><xsl:value-of select="notas/examen"></xsl:value-of></th>
                    
                    <xsl:if test="(notas/practicas+notas/examen)div 2 &lt; 5">
                        <th style="border: 1px solid black;background-color:red"><xsl:value-of select="(notas/practicas+notas/examen)div 2"></xsl:value-of></th>
                    </xsl:if>
                    <xsl:if test="(notas/practicas+notas/examen)div 2 &gt; 8">
                        <th style="border: 1px solid black;background-color:blue"><xsl:value-of select="(notas/practicas+notas/examen)div 2"></xsl:value-of></th>
                    </xsl:if>
                    <xsl:if test="(notas/practicas+notas/examen)div 2 &gt; 4 and (notas/practicas+notas/examen)div 2 &lt; 8">
                        <th style="border: 1px solid black"><xsl:value-of select="(notas/practicas+notas/examen)div 2"></xsl:value-of></th>
                    </xsl:if>
                    <th style="border: 1px solid black"><img src="{nombre}.png"/></th>
                    <!--<xsl:if test="notas/practicas &lt; 4 or notas/examen &lt; 4 ">
                        <th style="border: 1px solid black">4</th>
                    </xsl:if>
                    <xsl:if test="notas/practicas &gt; 4 or notas/examen &gt; 4 ">
                        <th style="border: 1px solid black"><xsl:value-of select="(notas/practicas+notas/examen)div 2"></xsl:value-of></th>
                    </xsl:if>-->
                    
                    
                    
                </tr>
            </xsl:for-each>
        </table>
    </body>
    </html>
    </xsl:template>
</xsl:stylesheet>