<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<lista>
    <xsl:apply-templates select="discos/group" />
</lista>
</xsl:template>
<xsl:template match="group">
    <xsl:variable name="nombre" select="name"></xsl:variable>
    <xsl:variable name="IdGroup" select="@id"></xsl:variable>
    <xsl:for-each select="//disco[interpreter/@id=$IdGroup]">
    <disco><xsl:value-of select="title"></xsl:value-of>
        es interpretado por:
    <xsl:value-of select="$nombre"/>
    </disco>
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>