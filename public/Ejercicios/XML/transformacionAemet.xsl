<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="root">
    <html>
    <body>
        <h1>AEMET</h1>
        <table style="border: 1px solid black">
            <tr>
                <th style="border: 1px solid black">Fecha</th>
                <th style="border: 1px solid black">Maxima</th>
                <th style="border: 1px solid black">Minima</th>
                <th style="border: 1px solid black">Prediccion</th>
            </tr>
            <xsl:for-each select="prediccion/dia">
            <xsl:sort select="temperatura/maxima" order="descending"></xsl:sort>
            <tr>
                <th style="border: 1px solid black"><xsl:value-of select="@fecha"></xsl:value-of></th>
                <th style="border: 1px solid black"><xsl:value-of select="temperatura/maxima"></xsl:value-of></th>
                <th style="border: 1px solid black"><xsl:value-of select="temperatura/minima"></xsl:value-of></th>
                <th style="border: 1px solid black">
                    <img src="{concat('imagenes/',estado_cielo[@periodo='00-24']/@descripcion)}.png" style="width;100px;height:100px"/>
                </th>
            </tr>
            </xsl:for-each>
        </table>
    </body>
    </html>
    </xsl:template>
</xsl:stylesheet>