<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<cadena>
    <nom><xsl:value-of select="programacio/audiencia/cadenes/cadena[@nom='Un TV']/@nom"></xsl:value-of></nom>
    <programas>
        <xsl:apply-templates />
    </programas>
</cadena>
</xsl:template>

<xsl:ttemplate match="programacio/audiencia">

        <programa>
            <xsl:attribute name="hora">
                <xsl:value-of select="hora"/>
            </xsl:attribute>
            <xsl:for-each select="cadenes">

                <nom-programa>
                    <xsl:value-of select="cadena[@nom='Un TV']"></xsl:value-of>
                </nom-programa>
                <audiencia>
                    <xsl:value-of select="cadena[@nom='Un TV']/@percentatge"></xsl:value-of>
                </audiencia>
                
            </xsl:for-each>
        </programa>


</xsl:template>


<!-- <xsl:template match="cadenes">
    <nom-programa><xsl:value-of select ="cadena[@nom='Un TV']"/></nom-programa>
    <audiencia><xsl:value-of select="cadena[@nom='Un TV']/@percentatge"/></audiencia>
</xsl:template> -->

</xsl:stylesheet>