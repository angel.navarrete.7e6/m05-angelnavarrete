<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
    <xsl:for-each select="videojuegos/videojuego">
    <article class="videojuegos">
        <img>
            <xsl:attribute name="src">
                <xsl:value-of select="infoGeneral/foto"/>
            </xsl:attribute>
            <xsl:attribute name="alt">
                <xsl:value-of select="nombre"></xsl:value-of>
            </xsl:attribute>
        </img>
        <h3><xsl:value-of select="nombre"></xsl:value-of></h3>
        <div class="descripcion">
            <xsl:value-of select="infoGeneral/descripcion"></xsl:value-of>
        </div>
        <a class="boton" >
            <xsl:attribute name="href">
                <xsl:value-of select="infoGeneral/enlace"></xsl:value-of>
            </xsl:attribute>
            Ver más
        </a>
    </article>
    </xsl:for-each>


</xsl:template>
</xsl:stylesheet>