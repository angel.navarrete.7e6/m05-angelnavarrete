<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/videojuego">

    <h2><xsl:value-of select="nombre" /></h2>
        <div class="imagenLogoJuego">
        <img>
            <xsl:attribute name="src">
                <xsl:value-of select="infoGeneral/foto"/>
            </xsl:attribute>
            <xsl:attribute name="alt">
                <xsl:value-of select="nombre"></xsl:value-of>
            </xsl:attribute>
        </img>
    </div>
        
        <h3><span>Generos: </span></h3>
        <p><xsl:value-of select="genero/@subGen"/>,<xsl:value-of select="genero/subGenero"/></p>
        <h3><span>Modo de juego: </span></h3>
        <p><xsl:value-of select="modosJuego/modoJuego"/></p>
        <h3 class="descripcin"><span>Descripcion: </span></h3>
        <div class="descripcion">
            <xsl:value-of select="infoGeneral/descripcion"></xsl:value-of>
        </div>
        <h3><span>Desarollador: </span><xsl:value-of select="infoGeneral/desarrollador" /></h3>
        <img class="logoEmpresa">
            <xsl:attribute name="src">
                <xsl:value-of select="infoGeneral/desarrollador/@logo"/>
            </xsl:attribute>
            <xsl:attribute name="alt">
                <xsl:value-of select="infoGeneral/desarrollador"></xsl:value-of>
            </xsl:attribute>
        </img>
        <h3><span>Distribuidor:</span> <xsl:value-of select="infoGeneral/distribuidor" /></h3>
        <img class="logoEmpresa">
            <xsl:attribute name="src">
                <xsl:value-of select="infoGeneral/distribuidor/@logo"/>
            </xsl:attribute>
            <xsl:attribute name="alt">
                <xsl:value-of select="infoGeneral/distribuidor"></xsl:value-of>
            </xsl:attribute>
        </img>

        <br></br>
        <a class="boton" >
            <xsl:attribute name="href">
                <xsl:value-of select="enlace"></xsl:value-of>
            </xsl:attribute>
            Ver más
        </a>
        <br></br>
        <h3><span>Idiomas:</span> </h3>
        <p><xsl:value-of select="infoGeneral/idiomas"/></p>
        <h3><span>Plataformas:</span> </h3>
        <ul>
            <xsl:for-each select="plataformas/plataforma">
                <li><xsl:value-of select="."/>, Fecha de Salida: <xsl:value-of select="./@fechaSalida"/>, Precio: <xsl:value-of select="./@precio"/></li>
            </xsl:for-each>
        </ul>
</xsl:template>

</xsl:stylesheet>