//Exercici 1
function CambiarColor(){
    var txt = document.getElementById("text");
    txt.style.color = "red";
}

//Exercici 2
function PrenValors(){
    formulari = getElementById("formulari1");
    //console.log(formulari);
    console.log(formulari["nom"].value,formulari["cognom"].value);
    alert(formulari["nom"].value,formulari["cognom"].value);
}
//Exercici 3
function changePBackground() {
    paragrafs = document.querySelectorAll("p");
    for (var para of paragrafs) {
        para.style.background = "red";
    }
}
//Exercici 4
function obtenirAtributs(){
    let enllacA = document.querySelector("p>a").attributes;
    console.log(enllacA);
}
//Exercici 5
function insertarFilaInici() {
    let taula = document.getElementById("Taula");
    let primerFilaTaula = taula.getElementsByTagName("tr")[0]
    let filaNova = document.createElement("tr");
    for (let i = 0; i < 2; i++) {
        let td1 = document.createElement("td");
        let content = document.createTextNode("Fila Nova");
        td1.appendChild(content);  
        filaNova.appendChild(td1); 
    }
    primerFilaTaula.before(filaNova);
}
function insertarFilaFinal() {
    let taula = document.getElementById("Taula");
    let files = taula.getElementsByTagName("tr");
    let ultimaFila = files[files.length-1]
    let filaNova = document.createElement("tr");
    for (let i = 0; i < 2; i++) {
        let td1 = document.createElement("td");
        let content = document.createTextNode("Fila Nova");
        td1.appendChild(content);  
        filaNova.appendChild(td1); 
    }
    ultimaFila.after(filaNova);
}
//Exercici 6
function insertValue(){
    let fila = document.getElementById("fila").value-1
    let cela = document.getElementById("cela").value-1
    let valors = document.getElementById("valor").value
    let updateCela = document.getElementById("Taula").getElementsByTagName("tr")[fila].getElementsByTagName("td")[cela]
    if(updateCela != null){
        updateCela.innerHTML = valors
        fila.value = ""
        cela.value = ""
        valors.value = ""
    }else{
        alert("Cel·a no existeix")
    }
}
 