function repeatChr(palabra, repeticiones){
    let resultadoFinal = "";
    for (let i = 0; i < palabra.length; i++) {
        resultadoFinal += palabra[i].repeat(repeticiones);
    }
    return resultadoFinal;
}

console.log(repeatChr("alex", 3)) //➞ "aaallleeexxx"
console.log(repeatChr("waytolearnx", 1)) //➞ "waytolearnx"
console.log(repeatChr("bob", 2)) //➞ "bboobb"


