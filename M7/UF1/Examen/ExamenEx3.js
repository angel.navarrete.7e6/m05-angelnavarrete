function calculaTip(precio, propina){
    switch(propina.toUpperCase()){
        case "EXCELENTE":
            return (precio*0.20).toFixed(0);
            break;
        case "ESTUPENDO":
            return (precio*0.15).toFixed(0);
            break;
        case "BUENO":
            return (precio*0.10).toFixed(0);
            break;
        case "EXCASO":
            return (precio*0.5).toFixed(0);
            break;
        case "MALO":
            return precio;
            break;
        default:
            return "Calificacion no reconocida";
            break;
    }
}

/*Malo: 100%
• Excaso: ​​50%
• Bueno: 10%
• Estupendo: 15%
• Excelente: 20%

POR CAMAREROS PARA CAMAREROS
*/
console.log(calculaTip(20, "ExceLEnte")); // --> 4 
console.log(calculaTip(26.95, "bueno")); // --> 3 // 2.695 redondeado a 3
console.log(calculaTip(89, "Malisimo")); // --> "Calificación no reconocida"


