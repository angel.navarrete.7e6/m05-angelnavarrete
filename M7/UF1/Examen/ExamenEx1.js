function prodImp (array){
    let resultado= 1;
    for (const num of array) {
        if (num%2==1){
            resultado *= num;
        }
    }
    return resultado;
}
console.log(prodImp([1, 2, 3, 4])) // -->3
console.log(prodImp([3, 6, 8, 5, 5, 7])) // --> 525
console.log(prodImp([1, 0, 1, 0, 1, 0])) // --> 1