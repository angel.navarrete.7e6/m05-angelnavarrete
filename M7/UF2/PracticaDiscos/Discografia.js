class Discografia {
    //ATRIBUTOS
    discos = [];

    //METODOS
    AnyadirDisco(Disco){
        this.discos.push(Disco);
    }
    BorrarDisco(Disco){
        for (const discoArray of this.discos) {
            if (discoArray.nombreDisco == this.discos.nombreDisco) {
                this.discos.pop(Disco);
            }
        }
    }
    OrdenarDiscos(parametro){
        try{
            this.discos.sort(function (a,b){return (b.parametro-a.parametro)});
        }catch(error){
            console.log("Parametro Incorrecto: "+ parametro);
        }
    }
    MostrarDiscos(){
        let tabla = document.getElementById("tablaDiscos");
        tabla.innerHTML+="<th>Nombre</th><th>Interprete</th><th>Año Publicacion</th><th>Tipo</th><th>URL</th><th>Localizacion</th><th>Prestado</th>";
        for (const disco of this.discos) {
            tabla.innerHTML+= "<td>"+disco.nombreDisco+"</td><td>"+disco.Interprete+"</td><td>"+disco.anoPublicacion+"</td><td>"+disco.tipo+"</td><td>"+disco.localizacion+"</td><td>"+disco.prestado +"</td>";
            console.log(disco.nombreDisco);
        }
    }
}