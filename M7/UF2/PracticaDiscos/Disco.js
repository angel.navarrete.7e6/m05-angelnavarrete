class Disco{
    //ATRIBUTOS
    nombreDisco;
    Interprete;
    anoPublicacion;
    tipo;
    url;
    #localizacion;
    #prestado;

    //CONSTRUCTOR
    constructor(){
        this.nombreDisco = "";
        this.Interprete = "";
        this.anoPublicacion = 0;
        this.tipo = [];
        this.url = "";
        this.#localizacion = 0;
        this.#prestado = false;

    };
    EditarDisco(nombreDisco,Interprete,anoPublicacion, tipo, url, localizacion){
        this.nombreDisco= nombreDisco;
        this.Interprete = Interprete;
        this.anoPublicacion = anoPublicacion;
        this.tipo = tipo;
        this.url = url;
        this.localizacion = localizacion;
        this.prestado = false;
    }
    
    //METODOS
    MostrarDisco() {
        return this.nombreDisco + " "+this.Interprete + " "+this.anoPublicacion+" "+this.tipo+" "+this.url+" "+this.localizacion+ " "+this.prestado;
    }
}