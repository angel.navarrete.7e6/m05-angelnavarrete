class Interprete {
    //ATRIBUTOS
    #nombre;
    #apellidos;
    #nombreArtistico;

    //CONSTRUCTOR
    constructor(nombre, apellidos, nombreArtistico){
        this.#nombre = nombre;
        this.#apellidos = apellidos;
        this.#nombreArtistico = nombreArtistico;
    }
    //GETTERS Y SETTERS
    get Interprete(){
        return this.#nombre + ' ' + this.#apellidos + '\n' + this.#nombreArtistico;
    }
    set Interprete(itr) {
        this.#nombre = itr.split(",")[1];
        this.#apellidos = itr.split(",")[0];
        this.#nombreArtistico = itr.split(",")[2];
    }
}